package net.resiststan.puzzle15example;

import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.PreferenceManager;

import com.resiststan.puzzle15example.R;

import net.resiststan.puzzle15example.util.android.CustomAppCompatActivity;

import java.util.Objects;

public class SettingsActivity extends CustomAppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        setSupportActionBar(findViewById(R.id.toolbar));
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true); //
        getSupportActionBar().setTitle(getResources().getString(R.string.settings));

        //set default value first run app
        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);

        if (findViewById(R.id.container) != null) {

            getSupportFragmentManager()

                    .beginTransaction()
                    .add(R.id.container, new SettingsFragment())
                    .commit();
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(getResources().getString(R.string.pref_key_theme))) {
            setTheme(sharedPreferences);
            recreate();
        } else if (key.equals(getResources().getString(R.string.pref_key_orientation))) {
            setOrientation(sharedPreferences);

            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                recreate();
            }
        } else if (key.equals(getResources().getString(R.string.pref_key_language))) {
            setLanguage(sharedPreferences);
            recreate();
        }
    }

    public static class SettingsFragment extends PreferenceFragmentCompat {

        @Override
        public void onCreatePreferences(Bundle bundle, String rootKey) {
            setPreferencesFromResource(R.xml.preferences, rootKey);
        }
    }
}
