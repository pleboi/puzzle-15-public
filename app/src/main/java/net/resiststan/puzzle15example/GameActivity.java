package net.resiststan.puzzle15example;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.resiststan.puzzle15example.R;

import net.resiststan.puzzle15example.util.android.MyOnTouchListener;
import net.resiststan.puzzle15example.contract.IContract;
import net.resiststan.puzzle15example.presenter.GameFieldPresenter;
import net.resiststan.puzzle15example.util.android.CustomAppCompatActivity;
import net.resiststan.puzzle15example.util.data.Manager;
import net.resiststan.puzzle15example.util.ui.GameField;

import java.io.Serializable;

public class GameActivity extends CustomAppCompatActivity implements IContract.IGame.IView {

    private static final String EXTRA_FIELD_SIDE = "com.example.myapplication.GameActivity.fieldSize";
    private static final String EXTRA_GAME_TYPE = "com.example.myapplication.GameActivity.fieldTypeCode";
    private static final String EXTRA_RESUME_GAME = "com.example.myapplication.GameActivity.resumeGame";
    private static final String SER_PRES_IMPL = "serPresImpl"; // key for presenter in InstanceState

    private TextView[] arrTextView;
    private IContract.IGame.IPresenter presenter;
    private String gameType;
    private int fieldSide;
    private boolean saveGame;


    public static Intent makeIntentNewGame(Context context, int size, String gameType) {
        Intent intent = new Intent(context, GameActivity.class);
        intent.putExtra(EXTRA_FIELD_SIDE, size);
        intent.putExtra(EXTRA_GAME_TYPE, gameType);
        return intent;
    }

    public static Intent makeIntentResumeGame(Context context, Serializable resumeGame) {
        Intent intent = new Intent(context, GameActivity.class);
        intent.putExtra(EXTRA_RESUME_GAME, resumeGame);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_game);

        setSupportActionBar(findViewById(R.id.toolbar));

        initView(savedInstanceState);
    }

    private void initView(Bundle savedInstanceState) {
        //extract date from intent
        saveGame = true;
        Intent intent = getIntent();

        if (intent.getSerializableExtra(EXTRA_RESUME_GAME) != null) {
            GameFieldPresenter serPresImpl = (GameFieldPresenter) intent.getSerializableExtra(EXTRA_RESUME_GAME);
            fieldSide = serPresImpl.getFieldSide();
            gameType = serPresImpl.getFieldType();
            presenter = serPresImpl;
        } else {
            if (savedInstanceState != null && savedInstanceState.containsKey(SER_PRES_IMPL)) {
                GameFieldPresenter serPresImpl = (GameFieldPresenter) savedInstanceState.getSerializable(SER_PRES_IMPL);
                gameType = serPresImpl.getFieldType();
                fieldSide = serPresImpl.getFieldSide();
                presenter = serPresImpl;
            } else {
                int fieldSideCode = intent.getIntExtra(EXTRA_FIELD_SIDE, 4);
                gameType = intent.getStringExtra(EXTRA_GAME_TYPE);
                fieldSide = getFieldSide(fieldSideCode);
                presenter = new GameFieldPresenter(fieldSide, gameType);
            }

        }

        //draw field by intent
        initField(fieldSide);

        // final prepare view
        presenter.attachView(this);
        presenter.viewIsReady();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void setScoreboard(int best, int current) {
        TextView tvBS = findViewById(R.id.tvBestScore);
        TextView tvCS = findViewById(R.id.tvCurrentScore);

        tvBS.setText(String.valueOf(best));
        tvCS.setText(String.valueOf(current));
    }

    private int getFieldSide(int ind) {
        int[] a = getResources().getIntArray(R.array.field_side_values);

        return a[ind];
    }

    private void initField(int fieldSide) {
        arrTextView = new TextView[fieldSide * fieldSide];

        GameField gameField = new GameField(getResources().getDisplayMetrics(), getResources().getConfiguration().orientation, fieldSide);

        final TableLayout mainTableLayout = gameField.configTableLayout(findViewById(R.id.mainTableLayout));

        for (int i = 0; i < fieldSide; i++) {

            TableRow tr = gameField.createRow(this);
            mainTableLayout.addView(tr);

            for (int j = 0; j < fieldSide; j++) {
                TextView tv = gameField.createTextView(this);
                arrTextView[fieldSide * i + j] = tv;
                setMyOnTouchListener(tv);
                setMyOnClickListener(tv, fieldSide * i + j);
                tr.addView(tv);
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(SER_PRES_IMPL, (Serializable) presenter);

        super.onSaveInstanceState(outState);
    }


    private void setMyOnClickListener(View v, int ind) {
        v.setOnClickListener(view -> presenter.moveCell(ind));
    }

    private void startNewGame() {
        saveGame = true;
        presenter.startNewGame();
    }

    private void setMyOnTouchListener(View view) {
        view.setOnTouchListener(new MyOnTouchListener(GameActivity.this) {
            public void onRightToLeftSwipe() {
                presenter.onRightToLeftSwipe();
            }

            public void onLeftToRightSwipe() {
                presenter.onLeftToRightSwipe();
            }

            public void onTopToBottomSwipe() {
                presenter.onTopToBottomSwipe();
            }

            public void onBottomToTopSwipe() {
                presenter.onBottomToTopSwipe();
            }
        });
    }

    @Override
    public void clearCell(int ind) {
        arrTextView[ind].setAlpha(0);
        arrTextView[ind].setText(null);
    }

    @Override
    public void fillCell(int ind, String value) {
        arrTextView[ind].setAlpha(1);
        arrTextView[ind].setText(value);
    }

    @Override
    public void gameCompleted() {

        saveGame = false;

        new AlertDialog.Builder(this)
                .setTitle(R.string.completed_dialog_title)
                .setPositiveButton(R.string.start_new_game, (dialog, which) -> { // set button for start new game
                    startNewGame();
                })
                .setNegativeButton(R.string.go_to_main, (dialog, which) -> finish())
                .setCancelable(false)
                .show();
    }

    @Override
    public void updateScoreboard(int score) {
        TextView tvCS = findViewById(R.id.tvCurrentScore);
        tvCS.setText(String.valueOf(score));
    }

    @Override
    protected void onPause() {
        if (saveGame) {
            Manager.saveFile(this, getString(R.string.game_data), (Serializable) presenter);
        } else {
            Manager.deleteFile(this, getString(R.string.game_data));
        }

        super.onPause();
    }

    @Override
    public void setSupportActionBar(@Nullable Toolbar toolbar) {
        super.setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.game_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_new_game) {
            new AlertDialog.Builder(this)
                    .setTitle(R.string.start_new_game_dialog_title)
                    .setPositiveButton(R.string.yes, (dialog, which) -> startNewGame())
                    .setNegativeButton(R.string.no, (dialog, which) -> {
                    })
                    .show();
        }

        return true;
    }
}
