package net.resiststan.puzzle15example.score;

import java.util.Map;

public interface IStatisticsDAO {

    Map<GameKey, GameScore> getAll();

    GameScore getOne(GameKey name);

    void update(GameScore s);

    void clearData();

    void save();
}
