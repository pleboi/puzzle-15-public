package net.resiststan.puzzle15example.score;

import java.io.Serializable;
import java.util.Objects;

public class GameKey implements Serializable {

    private final String gameType;
    private final Integer side;

    public GameKey(String gameType, Integer side) {
        this.gameType = gameType;
        this.side = side;
    }

    public String getGameType() {
        return gameType;
    }

    public Integer getSide() {
        return side;
    }

    @Override
    public int hashCode() {
        return Objects.hash(gameType, side);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        if (!(obj instanceof GameKey)) {
            return false;
        }

        GameKey otherGameKey = (GameKey) obj;
        return Objects.equals(otherGameKey.gameType, this.gameType)
                && Objects.equals(otherGameKey.side, this.side);
    }

    @Override
    public String toString() {
        return gameType + side;
    }
}
