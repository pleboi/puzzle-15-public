package net.resiststan.puzzle15example.score;

import android.content.Context;
import android.support.annotation.NonNull;

import com.resiststan.puzzle15example.R;

import net.resiststan.puzzle15example.util.data.Manager;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

public class StatisticsImpl implements IStatisticsDAO, Serializable {
    private static final String FILE_NAME = "stat.data";
    private final String[] type;
    private final int[] side;
    private final transient Context context;

    private final @NonNull
    Map<GameKey, GameScore> mapStat;

    public StatisticsImpl(Context context) {
        this.context = context;
        type = context.getResources().getStringArray(R.array.game_type);
        side = context.getResources().getIntArray(R.array.field_side_values);

        if (Manager.readObj(context, FILE_NAME) != null) {
            this.mapStat = ((StatisticsImpl) Manager.readObj(context, FILE_NAME)).getAll();
        } else {
            this.mapStat = new LinkedHashMap<>();
            initEmptyMap();
        }
    }

    private void initEmptyMap() {
        for (String t : type) {
            for (Integer s : side) {
                GameKey gameKey = new GameKey(t, s);
                mapStat.put(gameKey, new GameScore(gameKey));
            }
        }
    }

    @Override
    public Map<GameKey, GameScore> getAll() {
        return mapStat;
    }

    @Override
    public GameScore getOne(GameKey key) {
        if (mapStat.containsKey(key)) {
            return Objects.requireNonNull(mapStat.get(key)).clone();
        }

        return null;
    }

    @Override
    public void update(GameScore s) {
        mapStat.put(s.getKey(), s.update());
        save();
    }

    @Override
    public void clearData() {
        initEmptyMap();
        save();
    }

    @Override
    public void save() {
        Manager.saveFile(context, FILE_NAME, this);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        for (Map.Entry<GameKey, GameScore> g : mapStat.entrySet()) {
            sb.append(String.format("%-15s%6d%6d\n", g.getKey().toString(), g.getValue().getBest(), g.getValue().getLast()));
        }

        return sb.toString();
    }
}
