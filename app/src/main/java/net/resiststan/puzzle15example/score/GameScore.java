package net.resiststan.puzzle15example.score;

import java.io.Serializable;

public class GameScore implements IScore, Serializable {
    private GameKey key;
    private int bestScore;
    private int lastScore;
    private int currentScore;

    public GameScore(GameKey key) {
        this.key = key;
        this.bestScore = 0;
        this.lastScore = 0;
        this.currentScore = 0;
    }

    private GameScore(GameKey key, int bestScore, int lastScore) {
        this.key = key;
        this.bestScore = bestScore;
        this.lastScore = lastScore;
        this.currentScore = 0;
    }

    @Override
    public GameKey getKey() {
        return key;
    }

    @Override
    public int getBest() {
        return bestScore;
    }

    @Override
    public int getLast() {
        return lastScore;
    }

    @Override
    public int getCurrent() {
        return currentScore;
    }

    @Override
    public int increase() {
        return ++currentScore;
    }

    @Override
    public GameScore update() {
        if (bestScore > currentScore || bestScore == 0) {
            bestScore = currentScore;
        }
        lastScore = currentScore;
        currentScore = 0;

        return this;
    }

    @Override
    public GameScore clone() {
        return new GameScore(getKey(), getBest(), getLast());
    }

    @Override
    public String toString() {
        return "GameScore{" +
                "key='" + key + '\'' +
                ", bestScore=" + bestScore +
                ", lastScore=" + lastScore +
                ", currentScore=" + currentScore +
                '}';
    }
}