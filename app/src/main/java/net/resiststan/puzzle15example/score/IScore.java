package net.resiststan.puzzle15example.score;

public interface IScore {
    GameKey getKey();

    int getBest();

    int getLast();

    int getCurrent();

    int increase();

    GameScore update();
}
