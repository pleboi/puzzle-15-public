package net.resiststan.puzzle15example.util.ui;

import android.content.Context;

import com.resiststan.puzzle15example.R;

import net.resiststan.puzzle15example.score.GameKey;
import net.resiststan.puzzle15example.score.GameScore;
import net.resiststan.puzzle15example.score.IStatisticsDAO;

import java.util.Map;

public class ScoreBoard {
    public static String scoreBoard(IStatisticsDAO statistics, Context context) {
        StringBuilder sb = new StringBuilder();
        Map<GameKey, GameScore> all = statistics.getAll();

        String curTypeName = "";

        String[] types = context.getResources().getStringArray(R.array.game_type); // (*1)

        for (Map.Entry<GameKey, GameScore> g : all.entrySet()) {

            if (!curTypeName.equals(g.getKey().getGameType())) {
                curTypeName = g.getKey().getGameType();

                //костыль, чтоб в счет не попадала статистика по изображениям
                if (types[1].equals(curTypeName)) {
                    break;
                }

                //выводит тип игра, оставить (*1)
                /*int strId = context.getResources().getIdentifier(curTypeName, "string", context.getPackageName());
                sb.append(context.getResources().getString(strId) + "\n");
                if (sb.length() != 0) {
                    sb.append("\n");
                }*/

                sb.append(String.format("%-15s%-15s%-15s\n",
                        context.getResources().getString(R.string.lbl_field),
                        context.getResources().getString(R.string.best),
                        context.getResources().getString(R.string.last))
                );
            }

            //это какой то костыть, надо переделать, сфелать красивую форму
            String f = g.getKey().getSide() + "x" + g.getKey().getSide();
            String b = String.valueOf(g.getValue().getBest());
            String l = String.valueOf(g.getValue().getLast());
            int t1 = -20 + (f.length() - 1);
            int t2 = -15 + (b.length() - 1);
            int t3 = -20 + (l.length() - 1);
            String format = "%" + t1 + "s %" + t2 + "s   %" + t3 + "s\n";
            sb.append(String.format(format, f, b, l));
        }

        return sb.toString();
    }
}
