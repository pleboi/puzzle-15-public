package net.resiststan.puzzle15example.util.ui;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.resiststan.puzzle15example.R;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;
import static android.os.Build.VERSION.SDK_INT;
import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

public class GameField {

    private static final int MIN_TEXT_SIZE = 12;
    private static final int MAX_TEXT_SIZE = 120;
    private static final int SIZE_STEP_GRANULARITY = 2;
    private final DisplayMetrics displayMetrics;
    private final int fieldSide; //cell in row
    private final int orientation;
    private int textSize = 12;
    private int pxFieldSize; //field size in pixels

    public GameField(DisplayMetrics displayMetrics, int orientation, int fieldSide) {
        this.displayMetrics = displayMetrics;
        this.fieldSide = fieldSide;
        this.orientation = orientation;
    }

    private void calcFieldSize(TableLayout tableLayout) {
        pxFieldSize = (displayMetrics.heightPixels < displayMetrics.widthPixels ? displayMetrics.heightPixels : displayMetrics.widthPixels);// - tableLayout.getPaddingRight() + tableLayout.getPaddingLeft();

        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            pxFieldSize -= 100 * displayMetrics.density;
        }

        textSize = (int) (pxFieldSize / (displayMetrics.density * fieldSide * 2));
    }

    public TableLayout configTableLayout(TableLayout tableLayout) {
        calcFieldSize(tableLayout);

        LinearLayout.LayoutParams ll = new LinearLayout.LayoutParams(pxFieldSize, pxFieldSize);
        tableLayout.setLayoutParams(ll);

        return tableLayout;
    }

    public TableRow createRow(Context context) {
        TableRow tr = new TableRow(context);
        tr.setLayoutParams(new TableLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT, 1.0f));
//        tr.setWeightSum(1.0f);
        tr.setId(View.generateViewId());

        return tr;
    }

    public TextView createTextView(Context context) {
        int m = 3;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        TextView tv = (TextView) inflater.inflate(R.layout.cell_prototype, null);

        TableRow.LayoutParams paramsTR = new TableRow.LayoutParams(MATCH_PARENT, MATCH_PARENT, 1.0f);
        paramsTR.setMargins(m, m, m, m);
        tv.setLayoutParams(paramsTR);

        tv.setId(View.generateViewId());
        tv.setPadding(m, m, m, m);
        tv.setGravity(Gravity.CENTER);

        if (SDK_INT >= Build.VERSION_CODES.O) {
            tv.setAutoSizeTextTypeUniformWithConfiguration(
                    MIN_TEXT_SIZE, MAX_TEXT_SIZE,
                    SIZE_STEP_GRANULARITY,
                    TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM
            );
        } else {
            tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize);
        }

        return tv;
    }
}
